﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class SokobanGameManager : MonoBehaviour
{
    GameObject objProximo, objProximoProximo;
    Nivel nivel, nivelAux;
    GameObject casillero, casilleroTarget, pared, jugador, bloque /*laser*/;
   public  List<Vector2> posOcupadasEsperadasCasillerosTarget;
    public List<Vector2> posicionBloque;
    Stack pilaTablerosAnteriores;
   public List<Vector2> DondeDeberiaPonerLosBloques;
   
    string orientacionJugador;
    string nombreNivelActual = "Nivel1";
    bool gameOver = false;
    Stack<Tablero> pilaTablerosAnteiores = new Stack<Tablero>();
    //public List<Vector2> casilleroTargetPosicion;
   public  int cont;


    public static bool estoyDeshaciendo = false;

    private void Start()
    {
        casillero = SokobanLevelManager.instancia.dameLstPrefabsSokoban().Find(x => x.name == "Casillero");
        casilleroTarget = SokobanLevelManager.instancia.dameLstPrefabsSokoban().Find(x => x.name == "CasilleroTarget");
        pared = SokobanLevelManager.instancia.dameLstPrefabsSokoban().Find(x => x.name == "Pared");
        jugador = SokobanLevelManager.instancia.dameLstPrefabsSokoban().Find(x => x.name == "Jugador");
        bloque = SokobanLevelManager.instancia.dameLstPrefabsSokoban().Find(x => x.name == "Bloque");
        //laser = SokobanLevelManager.instancia.dameLstPrefabsSokoban().Find(x => x.name == "Laser");
        CargarNivel(nombreNivelActual);
    }

    private void CargarNivel(string nombre)
    {
        nivel = SokobanLevelManager.instancia.dameNivel(nombre);
        posOcupadasEsperadasCasillerosTarget = nivel.Tablero.damePosicionesObjetos("CasilleroTarget");
        DondeDeberiaPonerLosBloques = nivel.Tablero.damePosicionesObjetos("CasilleroTarget");
        posicionBloque = nivel.Tablero.damePosicionesObjetos("bloque");
        posicionBloque = nivel.Tablero.damePosicionesObjetos("Bloque");

        InstanciadorPrefabs.instancia.graficarCasilleros(nivel.Tablero, casillero);
        InstanciadorPrefabs.instancia.graficarCasillerosTarget(nivel.Tablero, casilleroTarget);
        InstanciadorPrefabs.instancia.graficarObjetosTablero(nivel.Tablero, SokobanLevelManager.instancia.dameLstPrefabsSokoban());
    }

    private void Update()
    {

        posOcupadasEsperadasCasillerosTarget = nivel.Tablero.damePosicionesObjetos("CasilleroTarget");
        posicionBloque = nivel.Tablero.damePosicionesObjetos("Bloque");

        if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
        {
            orientacionJugador = "derecha";
            mover();
        }
        if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))
        {
            orientacionJugador = "arriba";
            mover();
        }
        if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow))
        {
            orientacionJugador = "izquierda";
            mover();
        }
        if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
        {
            orientacionJugador = "abajo";
            mover();
        }
        if (Input.GetKeyDown(KeyCode.Z))
        {
            estoyDeshaciendo = true;
            mover();
        }

        //ChequearVictoria(nivel.Tablero);


    }

    private void mover()
    {
      

        if (estoyDeshaciendo == false)
        {
         
            Tablero tablAux = new Tablero(nivel.Tablero.casilleros.GetLength(0), nivel.Tablero.casilleros.GetLength(1));
            tablAux.setearObjetos(casillero, nivel.Tablero.damePosicionesObjetos("Casillero"));
            tablAux.setearObjetos(casilleroTarget, nivel.Tablero.damePosicionesObjetos("CasilleroTarget"));
            tablAux.setearObjetos(bloque, nivel.Tablero.damePosicionesObjetos("Bloque"));
            tablAux.setearObjetos(pared, nivel.Tablero.damePosicionesObjetos("Pared"));
            tablAux.setearObjetos(jugador, nivel.Tablero.damePosicionesObjetos("Jugador"));
            tablAux.setearObjetos(jugador, nivel.Tablero.damePosicionesObjetos("Laser"));

            //Vector2 posicionBloque = new Vector2(nivel.Tablero.damePosicionObjeto("bloque").x, nivel.Tablero.damePosicionObjeto("bloque").y);
            //Vector2 posicionTarget = new Vector2(nivel.Tablero.damePosicionObjeto("CasilleroTarget").x, nivel.Tablero.damePosicionObjeto("CasilleroTarget").y);

            //TIP: pilaTablerosAnteriores.Push(tablAux);

            Vector2 posicionJugador = new Vector2(nivel.Tablero.damePosicionObjeto("Jugador").x, nivel.Tablero.damePosicionObjeto("Jugador").y);
            //GameObject objProximo, objProximoProximo;

            //this.pilaTablerosAnteiores.Push(tablAux);

            if (objProximo == null /*&& objProximoProximo == null*/)
            {
                nivel.Tablero.setearObjeto(jugador, posicionJugador, orientacionJugador, 0);

            }
            else
            {
                this.pilaTablerosAnteiores.Push(tablAux);
            }

            if (orientacionJugador == "abajo" || orientacionJugador == "izquierda")
            {
                objProximo = nivel.Tablero.dameObjeto(posicionJugador, orientacionJugador, -1);
                objProximoProximo = nivel.Tablero.dameObjeto(posicionJugador, orientacionJugador, -2);
            }
            else
            {
                objProximo = nivel.Tablero.dameObjeto(posicionJugador, orientacionJugador, 1);
                objProximoProximo = nivel.Tablero.dameObjeto(posicionJugador, orientacionJugador, 2);
            }
            //hacer que no tire error al final del mapa
            if (objProximo == null && objProximoProximo == null)
            {
                nivel.Tablero.setearObjeto(jugador, posicionJugador, orientacionJugador, 0);
            }
            else
            if (objProximo != null && objProximo.CompareTag("casillero") )
            {
                nivel.Tablero.setearObjeto(casillero, posicionJugador);
                nivel.Tablero.setearObjeto(jugador, posicionJugador, orientacionJugador, 1);
            }
            else
            {
                if (objProximoProximo.CompareTag("bloque"))
                {
                    if (objProximo != null && objProximo.CompareTag("bloque") && objProximoProximo == null)
                    {
                        nivel.Tablero.setearObjeto(jugador, posicionJugador, orientacionJugador, 1);
                        {
                            nivel.Tablero.setearObjeto(casillero, posicionJugador);
                            nivel.Tablero.setearObjeto(bloque, posicionJugador, orientacionJugador, 2); ;
                        }
                    }
                }
                else
                {
                    if (objProximo != null && objProximo.CompareTag("bloque") && objProximoProximo != null)
                    {
                        nivel.Tablero.setearObjeto(jugador, posicionJugador, orientacionJugador, 1);
                        {
                            nivel.Tablero.setearObjeto(casillero, posicionJugador);
                            nivel.Tablero.setearObjeto(bloque, posicionJugador, orientacionJugador, 2); ;
                        }
                    }
                }
            }
            InstanciadorPrefabs.instancia.graficarObjetosTablero(nivel.Tablero, SokobanLevelManager.instancia.dameLstPrefabsSokoban());


           

            if (ChequearVictoria(nivel.Tablero) == true)
            {


                Debug.Log("Gané");
            }
        }
        else
        {




            //TIP: pilaTablerosAnteriores.Push(tablAux);

            //Vector2 posicionJugador = new Vector2(nivel.Tablero.damePosicionObjeto("Jugador").x, nivel.Tablero.damePosicionObjeto("Jugador").y);
            //GameObject objProximo, objProximoProximo;



            //objProximo = nivel.Tablero.dameObjeto(posicionJugador, orientacionJugador, -1);
            //objProximoProximo = nivel.Tablero.dameObjeto(posicionJugador, orientacionJugador, -2);

            //if (objProximo == nivel.Tablero.dameObjeto(posicionJugador, orientacionJugador, 0))
            //{
            //    Debug.Log("eres el mejor");
            //}


            //    objProximo = nivel.Tablero.dameObjeto(posicionJugador, orientacionJugador, 1);
            //    objProximoProximo = nivel.Tablero.dameObjeto(posicionJugador, orientacionJugador, 2);


            //pilaTablerosAnteriores.Push("derecha");
            //pilaTablerosAnteriores.Push("izquierda");
            //pilaTablerosAnteriores.Push("abajo");
            //pilaTablerosAnteriores.Push("arriba");



            //pilaTablerosAnteriores.Push(Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow));
            //pilaTablerosAnteriores.Push(Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow));
            //pilaTablerosAnteriores.Push(Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow));
           
           
            
                if (this.pilaTablerosAnteiores.Count > 0)
                {

                    nivel.Tablero = (Tablero)pilaTablerosAnteiores.Pop();
                    InstanciadorPrefabs.instancia.graficarObjetosTablero(nivel.Tablero, SokobanLevelManager.instancia.dameLstPrefabsSokoban());
                }
                estoyDeshaciendo = false;

        }
    }

    private bool SonIgualesLosVectores(Vector2 v1, Vector2 v2)
    {
        return (v1.x == v2.x && v1.y == v2.y);
    }

    public bool ChequearVictoria(Tablero tablero)
    {

        foreach (var bloque in posicionBloque)
        {

            foreach (var target in DondeDeberiaPonerLosBloques)
            {
                if (bloque == target)
                {
                    cont++;
                }
            }

        }
        if (cont == 3)
        {
            return true;
        }
        else
        {
            cont = 0;
        }
        return false;
        


        //if (posicionBloque.Contains(target) && )
        //{

        //    if (cont == 3)
        //    {
        //        return true;
        //    }

        //}


        //if (posOcupadasEsperadasCasillerosTarget.Count == 0 || DondeDeberiaPonerLosBloques == posicionBloque)
        //{
        //    Debug.Log("si se puedeeeee");
        //    return true;
        //}
        //else
        //{
        //    //Debug.Log("son distintos");
        //    return false;
        //}
        //return false;

        //foreach (var item in posOcupadasEsperadasCasillerosTarget)
        //{
        //    foreach ()
        //    {
        //        if (bloque == item) numerosDeCasilleros--;
        //    }
        //}
        //if (numerosDeCasilleros == 0) return true;
        //else numerosDeCasilleros = 3;
        //return false;

        //if (tablero.damePosicionesObjetos("Bloque") == tablero.damePosicionesObjetos("CasilleroTarget"))
        //{


        //    return true;

        //}
        //else
        //{
        //    return false;
        //}

        //posicionCasilleroTarget = new Vector2(nivel.Tablero.damePosicionObjeto("CasilleroTarget").x, nivel.Tablero.damePosicionObjeto("CasilleroTarget").y);

        // posicionBloques = new Vector2(tablero.damePosicionObjeto("Bloque").x, tablero.damePosicionObjeto("Bloque").y);






        //if (posOcupadasEsperadasCasillerosTarget == posicionBloque)
        //{
        //    Debug.Log("si se puedeeeee");
        //    return true;
        //}
        //else
        //{
        //    return false;
        //}


    }
}

