﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BulletController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("pared") || collision.gameObject.CompareTag("bloque") )
        {
            Destroy(this.gameObject);
        }
        if (collision.gameObject.CompareTag("jugador"))
        {
            SceneManager.LoadScene(0);
           
        }
    }
}
