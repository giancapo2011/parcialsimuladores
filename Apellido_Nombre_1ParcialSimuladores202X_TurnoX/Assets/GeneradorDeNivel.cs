﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class GeneradorDeNivel : MonoBehaviour
{
    public ColorAPrefab[] colorMappings;
    public GameObject reference;

    public List<ElementGame> elementoLista;

    public GameObject casillero;
    public GameObject casilleroTarget;
    public GameObject jugador;
    public GameObject bloque;
    public GameObject pared;

    public Tablero tablero;

    public SokobanLevelManager LvlManager;

    private void Awake()
    {
        LvlManager = GameObject.Find("SokobanLevelManager").GetComponent<SokobanLevelManager>();
    }

    public  void GenerateTile(int x, int y, Texture2D mapa)
    {
        Color pixelColor = mapa.GetPixel(x, y);

        if (pixelColor.a == 0)
        {
            return;
        }

        foreach (ColorAPrefab colorMapping in colorMappings)
        {
            if (colorMapping.color.Equals(pixelColor))
            {
                Vector2 position = new Vector2(x, y);
                LvlManager.tablero.setearObjeto(colorMapping.prefab, position);
            }
        }
        
    }
    

}
